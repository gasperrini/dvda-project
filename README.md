# Komunikacija

* [MS Teams Kaunas](https://teams.microsoft.com/l/team/19%3amOHvXiWzujD3I8iEZ1MSphu2quPCy3_EMNBCVHAPe6k1%40thread.tacv2/conversations?groupId=987533d9-627f-4fae-bbd2-7afc489208b3&tenantId=3415f2f7-f5a8-4092-b52a-003aaf844853)
* [MS Teams Vilnius](https://teams.microsoft.com/l/team/19%3axAOXsWkotXQgTwh5K866YgMm4JqTk9AKyAHWpEVvptc1%40thread.tacv2/conversations?groupId=6803d60f-9320-42d0-8f94-c2e3542aae53&tenantId=3415f2f7-f5a8-4092-b52a-003aaf844853)


# Užduotis

Sukurkite duomenų produktą - analitinę aplikaciją, skirtą banko paskolos įvertinimui mašininio mokymosi algoritmų pagalba

![Data Product](/slides/img/data_product.png){width=50}


#### Projekte ugdomi gebėjimai  

* analizuoti ir interpretuoti skaičiavimų rezultatus, įvertinti ar galima taikyti duomenų tyrybos metodą konkretiems duomenims ir jeigu ne, pasiūlyti kitą tinkamą metodą;  
* apibendrinti gautus rezultatus ir pateikti pagrįstas išvadas ir prognozes, rengti ataskaitas;  
* atlikti duomenų tyrybą panaudojant duomenų tyrybos programinę įrangą; 
* paaiškinti naudotų duomenų tyrybos metodų sąvokas ir teoriją ; 
* sudaryti duomenų tyrybos modelius realiems didiesiems verslo duomenims tirti.

 
#### Projekto vertinimo kriterijai 

1. Mokslinės literatūros tinkamumas projekto temai.  
2. Domenų tyrybos metodų parinkimas projekto uždavinių sprendimui. 
3. Sukurtų duomenų tyrybos modelių tikslumas ir kokybė 
4. Modelių programų kokybė. 
5. Gautų rezultatų patikimumas, interpretavimo teisingumas, išvadų pagrįstumas. 
6. Rekomendacijos sukurtų modelių tobulinimui. 
7. Projekto ataskaitos atitikimas nustatytiems reikalavimams.

#### Reikalingi įrankiai projektui

* R (shiny, tidyverse, rmarkdown)
* git versijavimas (https://bitbucket.org/account/signup/)
* h2o mašininio mokymosi platforma https://www.h2o.ai/
* projekto ataskaita rengiama markdown formatu (arba https://rmarkdown.rstudio.com/) 

--------------------------------------- 

# Vertinimo schema *(10 balų skalė)*

1. Duomenų nuskaitymas ir apjungimas (1 t.)
1. Duomenų žvalgomoji analizė (1 t.)
1. Modelio parinkimas (1 t.)
1. Hyperparametrų optimizavimas (2 t.)
1. WEB aplikacijos sukūrimas paskolos spėjimui (2 t.)
1. Modelio tikslumas naujiems duomenims (2 t.)

Sum = 9

#### Papildomi taškai

1. Shiny aplikacijos dizainas (1 t.)
1. Shiny aplikacijos funkcionalumas (1 t.)
1. Papildomų ML paketu taikymas (SparkML, SciKit-Learn, Keras, Tensorflow) (1 t.)
1. Shiny aplikacijos pateikimas (Docker, Shiny-Apps, Shiny-Server) (1 t.)

sum = 4

**Top 3 komandos/dalyviai gaus maksimalų įvertinimą**

![Vertinimo sistema](/slides/img/vertinimas.png){width=50}


# Duomenys

Paskolų įsipareigojimo nevykdymas (loan default)

* id - unikalus ID 
* y - ar įvykdytas paskolos įsipareigojimas (0 - TAIP, 1 - NE)
* kiti kintamieji - paskolos parametrai (pvz. kredito istorija, paskolos tipas)

Duomenis galite atsisiųsti iš:

[MS OneDrive](https://ktuedu-my.sharepoint.com/:f:/g/personal/kesdau_ktu_lt/EkbHuxm1KhBLvD7GXyrBTz4BY-OtB2d64EWCPCJ1NdmDww?e=go9C6u)

arba

[Google Drive](https://drive.google.com/drive/folders/17NsP84MecXHyctM94NLwps_tsowld_y8?usp=sharing)
