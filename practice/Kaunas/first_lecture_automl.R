# install.packages('h2o')
#test 
Sys.setenv(JAVA_HOME="C:/Program Files (x86)/Java/jre1.8.0_201")
library(h2o)
h2o.init()

df <- h2o.importFile("data/1-sample_data.csv")
df

y <- "y"
x <- setdiff(colnames(df), c(y, "id"))
x
df$y <- as.factor(df$y)
summary(df)

aml <- h2o.automl(x,
                  y,
                  df,
                  max_runtime_secs = 60)

aml@leaderboard
aml@leader

pred <- h2o.predict(aml@leader, df)




